import numpy as np
import cv2
import time
import RPi.GPIO as GPIO
from easygopigo3 import EasyGoPiGo3
import pyfirmata

go = EasyGoPiGo3()
    #SEE ON SERVO TEEMA
    #servoPIN = 17
    #GPIO.setmode(GPIO.BCM)
    #GPIO.setup(servoPIN, GPIO.OUT)
    #https://tutorials-raspberrypi.com/raspberry-pi-servo-motor-control/
#go.set_speed(0)
cap = cv2.VideoCapture(0)

    
def tagasi_koju():
    #cap = cv2.VideoCapture(0)
    global cap
    board = pyfirmata.Arduino('/dev/ttyUSB0')
    print("ok")

    left_servo = board.get_pin('d:10:s')
    right_servo = board.get_pin('d:9:s')
    
    left_servo.write(94)
    right_servo.write(94)

    lH=1
    def lHa(uus):
        global lH
        lH = uus
        return lH
    lS=41
    def lSa(uus):
        global lS
        lS = uus
        return lS
    lV=103
    def lVa(uus):
        global lV
        lV = uus
        return lV
    hH=12
    def hHa(uus):
        global hH
        hH = uus
        return hH
    hS=255
    def hSa(uus):
        global hS
        hS = uus
        return hS
    hV=255
    def hVa(uus):
        global lV
        hV = uus
        return hV
    cv2.namedWindow("Processed")
    cv2.createTrackbar("HueL", 'Processed', lH, 255, lHa)
    cv2.createTrackbar("SaturationL", 'Processed', lS, 255, lSa)
    cv2.createTrackbar("ValueL", 'Processed', lV, 255, lVa)
    cv2.createTrackbar("HueH", 'Processed', hH, 255, hHa)
    cv2.createTrackbar("SaturationH", 'Processed', hS, 255, hSa)
    cv2.createTrackbar("ValueH", 'Processed', hV, 255, hVa)

    Ret, fram = cap.read()

    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.filterByArea = 1
    blobparams.minArea = 150
    blobparams.maxArea = 100000
    blobparams.filterByCircularity = False
    blobparams.minDistBetweenBlobs = 150
    blobparams.filterByConvexity = 0
    blobparams.filterByInertia = 0
    blobparams.filterByColor = 0
    detector = cv2.SimpleBlobDetector_create(blobparams)
    while True:
        
        ret, fram = cap.read()
        fram = cv2.cvtColor(fram, cv2.COLOR_BGR2HSV)
        
        r=[len(fram)-190, len(fram)-130, 0, len(fram[0])]
        fram =fram[r[0]:r[1], r[2]:r[3]]
        fram = cv2.blur(fram,(13,13))
        
        kernel = np.ones((21,21),np.uint8)
        closing = cv2.morphologyEx(fram,cv2.MORPH_CLOSE, kernel)
        
        lowerLimits = np.array([lH, lS, lV])
        upperLimits = np.array([hH, hS, hV])
        
        thresholded = cv2.inRange(fram, lowerLimits, upperLimits)
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, kernel)
        
        outimage = cv2.bitwise_and(fram, fram, mask = thresholded)
        
        keypoints = detector.detect(thresholded)
        thresholded = cv2.bitwise_not(thresholded)
        fram = cv2.drawKeypoints(fram, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
           
        for keypoint in keypoints:
            cv2.putText(fram, str((round(keypoint.pt[0],2),round(keypoint.pt[1],2))), (int(keypoint.pt[0]),int(keypoint.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            roosa = (int(keypoints[0].pt[0]))
            print(roosa)
            
        if len(keypoints) ==  0:
            go.set_speed(30)
            go.left()
            
        elif len(keypoints) == 1:
            if roosa <= 290:
                go.set_speed(30)
                go.left()
                time.sleep(0.003)
                go.stop()
            elif roosa > 300:
                go.set_speed(30)
                go.right()
                time.sleep(0.003)
                go.stop()
            else:
                go.forward()
                time.sleep(0.05)
                go.stop()
        #cv2.putText(fram, str(aeg), (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        cv2.imshow("Original", fram)
        cv2.imshow("Processed", thresholded)
        
        if cv2.waitKey(1) & 0xFF == ord("q"):
            go.stop()
            break

    print("closing program")
    cap.release()
    cv2.destroyAllWindows()

    return True

def palli_tuvastus():
    #puhastaja servo funktsioonid
    global cap
    board = pyfirmata.Arduino('/dev/ttyUSB0')
    print("ok")

    left_servo = board.get_pin('d:10:s')
    right_servo = board.get_pin('d:9:s')

    #def forward():
    left_servo.write(0)
    right_servo.write(180)
        
    #cap = cv2.VideoCapture(0)

    #panin FPS ka sisse, just in case
    #start_time = time.time()
    
    #KAAMERA TUVASTAB ROHELIST
    lH=9
    def lHa(uus):
        global lH
        lH = uus
        return lH
    lS=71
    def lSa(uus):
        global lS
        lS = uus
        return lS
    lV=67
    def lVa(uus):
        global lV
        lV = uus
        return lV
    hH=71
    def hHa(uus):
        global hH
        hH = uus
        return hH
    hS=255
    def hSa(uus):
        global hS
        hS = uus
        return hS
    hV=255
    def hVa(uus):
        global hV
        hV = uus
        return hV

    cv2.namedWindow("Processed")
    cv2.createTrackbar("HueL", 'Processed', lH, 255, lHa)
    cv2.createTrackbar("SaturationL", 'Processed', lS, 255, lSa)
    cv2.createTrackbar("ValueL", 'Processed', lV, 255, lVa)
    cv2.createTrackbar("HueH", 'Processed', hH, 255, hHa)
    cv2.createTrackbar("SaturationH", 'Processed', hS, 255, hSa)
    cv2.createTrackbar("ValueH", 'Processed', hV, 255, hVa)

    Ret, frame = cap.read()

    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.filterByArea = 1
    blobparams.minArea = 150
    blobparams.maxArea = 100000
    blobparams.filterByCircularity = False
    blobparams.minDistBetweenBlobs = 150
    blobparams.filterByConvexity = 0
    blobparams.filterByInertia = 0
    blobparams.filterByColor = 0   
    detector = cv2.SimpleBlobDetector_create(blobparams)
    
    ajapiirang = time.time() + 5
    while True:
        
        #go.forward()
        #print("sõidab otse")
        #cv2.putText(frame, str(hetkel_eelmine), (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        ret, frame = cap.read()
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        r=[len(frame)-190, len(frame)-130, 0, len(frame[0])]
        frame =frame[r[0]:r[1], r[2]:r[3]]

        frame = cv2.blur(frame,(13,13))
        kernel = np.ones((21,21),np.uint8)
        closing = cv2.morphologyEx(frame,cv2.MORPH_CLOSE, kernel)
        
        lowerLimits = np.array([lH, lS, lV])
        upperLimits = np.array([hH, hS, hV])
        
        thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
        thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, kernel)
                
        outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
        
        keypoints = detector.detect(thresholded)
        thresholded = cv2.bitwise_not(thresholded)

        frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        for keypoint in keypoints:
            cv2.putText(frame, str((round(keypoint.pt[0],2),round(keypoint.pt[1],2))), (int(keypoint.pt[0]),int(keypoint.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        
            palli_asukoht = (int(keypoints[0].pt[0]))
            print(palli_asukoht)

        if len(keypoints) ==  0:
            if time.time() > ajapiirang:
                print(time.time())
                go.stop()
                tagasi_koju()
            else:
                go.set_speed(255)
                go.left()
                #time.sleep(0.05)
                #go.stop()
            
               # hetkel = 0
                
        elif len(keypoints) == 1:
            if palli_asukoht <= 290:
                #print('vasakule')
                go.set_speed(200)
                go.left()
                time.sleep(0.003)
                go.stop()
            elif palli_asukoht > 300:
                go.set_speed(200)
                go.right()
                time.sleep(0.003)
                go.stop()
            else:
                if keypoints[0].pt[1] < 50:
                    go.forward()
                    time.sleep(0.05)
                    go.stop()
                else:
                    go.stop()
                    time.sleep(2)
                    print('sõida pallini')# Siia panna see kordinaat, kus pall on pm kopa eest
                #print('sõida pallini ja siis peatu')
               # go.set_speed(200)
               # go.forward()
               # time.sleep(0.05)
               # go.stop()
            
        elif len(keypoints) > 1:
            if int(keypoints[0].pt[1]) > int(keypoints[1].pt[1]):
                #if int(keypoints[1].pt[1]) > int(keypoints[0].pt[1]):
                    go.forward()
                    time.sleep(0.05)
                    go.stop()
                    print('mine selle pallini mis on lähemal')
            
        cv2.imshow("Original", frame)
        cv2.imshow("Processed", thresholded)
        
        if cv2.waitKey(1) & 0xFF == ord("q"):
            go.stop()
            break

    print("closing program")
    #cv2.destroyAllWindows()
    
    return True


print(palli_tuvastus())
