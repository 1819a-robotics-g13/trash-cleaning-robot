import pyfirmata
import time

board = pyfirmata.Arduino('COM4')
print("ok")

left_servo = board.get_pin('d:10:s')
right_servo = board.get_pin('d:9:s')

def forward():
    left_servo.write(0)
    right_servo.write(180)
    
def stopRobot():
    left_servo.write(94)
    right_servo.write(95)

while True:
    forward()
    print("1")
    time.sleep(2)
    stopRobot()
    print("2")
    time.sleep(2)

