# Trash Cleaning Robot

Members:
	Kristi Maask
	Kristi Teder
	Kaspar Karro
	Karl Oskar Alamaa

Overview

Our project topic is to make a trash cleaning robot and in general 
the robot's mission is to clean the trash autonomously.

Further action:

There is trash all over the place in 4*4 meter area. Area is marked with 
tape. The trash is green paper balls. Trash location is marked with 
pink and it is located on the corner of the room. The robot has camera
attached to it. Trash position is detected by image processing. Robot 
also has a servo what is used to pick the trash. Servo is attached to 
cardboard ladle. Trash is collected in the box, which is made of paper 
and is located on the robot. After the robot has collected at least 6 
paper balls out of 10, it has to go to the collecting area in one go. 
The robot is fully autonomous while cleaning the trash and getting image 
processing results. 

Schedule for Pair A:

Week 1:
Writing the overview and the schedule
Week 2:
Trash cleaning algorithm
Week 3:
control the robot based on video feedback
Week 4: 
Combining the codes of two pairs and debugging the whole thing
Week 5:
Completion of the Poster
Week 6:
Poster session with live demo

Schedule for Pair B:

Week 1:
Writing the overview and the schedule
Week 2:
Area mapping
Week 3:
the robot tracking
Week 4:
Combining the codes of two pairs and debugging the whole thing
Week 5:
Completion of the Poster
Week 6:
Poster session with live demo

Component list:

The things we dont have and need to borrow

+-------------------------------+
|RaspberryPi                    | 1
+-------------------------------+
|Power adapter                  | 1
+-------------------------------+
|Sd-card                        | 1
+-------------------------------+
|Webcam                         | 1
+-------------------------------+
|General Servo                  | 2
+-------------------------------+
|The screen, with what          | 1
|we can see our IP              |
+-------------------------------+
|Gopigo2 robot                  | 1
+-------------------------------+
|Battery for Gopigo2            | 1
|and the voltmeter for battery  | 1
+-------------------------------+

Challenges and Solutions
List of problemsthat each one of us need to solve to complete our 
initial plan.

Kristi Maask
Work with the code, that the robot would go on a pink area.

Kaspar Karro
Work with the code that it would count the time properly. 

Kristi Teder
Work with the code that the servo would work with the Raspberry 
properly.

Karl Oskar Alamaa
Work with the code generaly, fixing bugs.

Everyone would also do some engineering with the robot.
